export const state = () => ({
  locales: ['en', 'fr'],
  locale: 'en'
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale;
    }
  }
}

export const actions = {
//  async nuxtServerInit({ dispatch }) {
//    const user_status = await this.$axios.$get('/user/status');
//    if (user_status['user_status'] == 1) {
//      const user = await this.$axios.$get('/user');
//    }
//  }
}
