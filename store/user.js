// state
export const state = () => {
  return {
    info: null,
  };
}

// getters
export const getters = {
    info: state => state.info,
}

// mutations
export const mutations = {
  fetch_info_success (state, s) {
    state.info = s;
  },
}

export const actions = {
  fetch_info ({ commit, state }) {
    if (null == state.info) {
      this.$gitlabcom.get('/api/v4/user').then(resp => {
        commit('fetch_info_success', resp);
      }).catch((e) => { console.log(e); });
    }
  },
}
