// state
export const state = () => {
  return {
    symbols: null,
  };
}

// getters
export const getters = {
    symbols: state => state.symbols,
}

// mutations
export const mutations = {
  fetch_symbols_success (state, s) {
    state.symbols = s;
  },
}

export const actions = {
  fetch_symbols ({ commit, state }) {
    if (null == state.symbols) {
      this.$axios.$get('/api/symbols').then((resp) => {
        commit('fetch_symbols_success', resp);
      });
    }
  },
}
