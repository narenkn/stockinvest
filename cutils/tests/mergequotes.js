const fs = require('fs');

var conf = {
  "analysis": "Invest::Dailyma-AlokJain-2",
  "dma_periods": [200, 100, 50, 20],
  "cross_margin": 5,
  "value_mark": 10000000,
  "symbols": {},
};

fs.readdirSync('../quote').forEach(fname => {
  const data = fs.readFileSync('../quote/' + fname, 'utf8');
  conf.symbols[fname.slice(0, -5)] = JSON.parse(data);
});

/* make all of them equal length */
let dkeys = {};
Object.keys(conf.symbols).forEach(s => {
  conf.symbols[s].ohlc.map(o => o.dt).forEach(dk => {dkeys[dk] = 0;});
});
const dates = Object.keys(dkeys);
const dateslen = dates.length;

/* make all data of equal length */
Object.keys(conf.symbols).forEach(s => {
  const tdates = conf.symbols[s].ohlc.map(o => o.dt);
  if (tdates.length == dates.length) return;
  dates.forEach(date => {
    if (tdates.includes(date)) return;
    conf.symbols[s].ohlc.push({
      dt: date,
      o: 0,
      h: 0,
      l: 0,
      c: 0,
      totqty: 0,
      totvalue: 0,
      tottrades: 0,
    });
  });
  /* sort */
  conf.symbols[s].ohlc.sort((a,b) => {
    return (a.dt < b.dt) ? -1 : (a.dt > b.dt) ? 1 : 0;
  });
  /* get first non-zero */
  let oc = {c: 0};
  for (let oci=0; 0 == oc.c; oci++) {
    oc = conf.symbols[s].ohlc[oci];
  }
  /* fill up the gaps */
  for (let oci=0; oci<dateslen; oci++) {
    if (0 == conf.symbols[s].ohlc[oci].c) {
      conf.symbols[s].ohlc[oci].c = oc.c;
      conf.symbols[s].ohlc[oci].totvalue = oc.totvalue;
    } else
      oc = conf.symbols[s].ohlc[oci];
  }
});

fs.writeFile('in2.json', JSON.stringify(conf), 'utf8', function (err) {
  if (err) {
    return console.log(err);
  }
});
