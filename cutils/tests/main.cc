#include <string>
#include <vector>
#include <iostream>

#include <boost/algorithm/string.hpp>

#include "json.hpp"

extern std::string
genmain(std::string s_config);

extern void
seed(uint32_t seed_val);

int
main(int argc, char *argv[])
{
  std::string s_config {R"({
                      "analysis": "Invest::Dailyma-AlokJain-2"
                     } )"};
  seed(3);
  auto ret = genmain(s_config);

  std::cout << ret << "\n";

  return 0;
}
