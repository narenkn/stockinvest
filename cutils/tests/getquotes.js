const axios = require('axios');
const fs = require('fs');

const BASE_URL = 'https://ss.dollane.com';

let symboldata = null;
const sleep = ms => new Promise(r => setTimeout(r, ms));

(async () => {
  symboldata = (await axios.get(BASE_URL + '/api/symbols', {params: {sector: 100}})).data;
  const symbols = Object.keys(symboldata);
  for (let si=0; si<symbols.length; si++) {
    const symb = symbols[si];
    symboldata[symb].ohlc = [];
    console.log(symb);
    for (let year=2016; year<2023; year++) {
      await axios.get(BASE_URL + '/api/ohlc', {params: {
        symbol_id: symboldata[symb].id,
        year: year,
      }}).then(d => {
        symboldata[symb].ohlc.push(... d.data.slice());
      }).catch(e => {
        console.log(e);
      });
    }

    fs.writeFile(`quote/${symb}.json`, JSON.stringify(symboldata[symb]), 'utf8', function (err) {
      if (err) {
        return console.log(err);
      }
    });
    await sleep(15000);
  }
})();
