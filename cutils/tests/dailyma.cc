#include <string>
#include <random>
#include <fstream>
#include <iostream>
#include <gtest/gtest.h>

#include <boost/algorithm/string.hpp>

#include "json.hpp"
#include "genmain.h"

extern std::string
genmain(std::string s_config);

extern void
seed(uint32_t seed_val);

class DailymaTest : public ::testing::Test {
public:
  Invest::Dailyma a1;

  void testOutputJson(nlohmann::json&, nlohmann::json&);

  // void SetUp() {
  // }
  // void TearDown() {
  // }
};

void
DailymaTest::testOutputJson(nlohmann::json& conf, nlohmann::json& retj)
{
}

TEST_F(DailymaTest, ReturnsMultiinputTest) {
  std::ifstream f("../tests/in3.json");
  std::stringstream ss;

  ss << f.rdbuf(); //read the file
  std::string ret1 = genmain(ss.str());

  ASSERT_TRUE(false) << ret1;
  ASSERT_TRUE(nlohmann::json::accept(ret1));
  // auto retj = nlohmann::json::parse(ret1);
  // auto conf = nlohmann::json::parse(conf_1);
  // testOutputJson(conf, retj);
}

int
main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
