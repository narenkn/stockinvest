#include <emscripten/bind.h>

extern std::string
genmain(std::string s_config);

extern void
seed(uint32_t seed_val);

EMSCRIPTEN_BINDINGS(emmain) {
  emscripten::function("genmain", &genmain);
  emscripten::function("seed", &seed);
}
