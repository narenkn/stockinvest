#include <random>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <numeric>
#include <algorithm>
#include "json.hpp"
#include "genmain.h"

std::string
Invest::Dailyma::gen(nlohmann::json& config)
{
  /* */
  nlohmann::json ret;

  /* choose one test type randomly */
  if ("Invest::Dailyma-AlokJain-2" == config["analysis"])
    dma_alokjain_2(ret, config);
  else if ("Invest::Dailyma-AbsMomentum" == config["analysis"])
    dma_alokjain_2(ret, config);

  return ret.dump();
}

double
calcdma0(std::vector<double>& vals, uint32_t len, uint32_t period)
{
  if (len < period) {
    double sum = accumulate(vals.begin(), vals.end(), 0);
    return sum / (len+1); /* make it < average */
  }
  double sum = accumulate(vals.rbegin(), vals.rbegin()+period, 0);
  return sum / period;
}

double
calcdma(std::vector<double>& vals, uint32_t len, uint32_t period)
{
  return uint32_t(calcdma0(vals, len, period) * 100) / 100;
}

/*  */
void
calcmomentum( nlohmann::json& momobj,
              std::vector<std::string>& symbols,
              const nlohmann::json& config, const uint32_t di,
              const uint32_t odi )
{
  /* calculate every momentum */
  momobj = nlohmann::json::object();
  for (auto &symbol: symbols) {
    double nval = config["symbols"][symbol]["ohlc"][di]["c"].get<double>();
    double oval = config["symbols"][symbol]["ohlc"][odi]["c"].get<double>();
    momobj[symbol] = (oval > 0) ? uint32_t((nval-oval) * 100 / oval) : 0;
  }
}

// ----------------------------------------------------------------------
// Given the year, month and day, return the day number.
// (see: https://alcor.concordia.ca/~gpkatch/gdate-method.html)
// ----------------------------------------------------------------------
uint32_t
dayNumFromDate(uint32_t y, uint32_t m, uint32_t d)
{
  m = (m + 9) % 12;
  y -= m / 10;
  uint32_t dn = 365*y + y/4 - y/100 + y/400 + (m*306 + 5)/10 + (d - 1);

  return dn;
}

/*
 * Strategy : Absolute Momentum using Daily Moving Average - Alok Jain
 *   Buy : Cross 200 DMA + 5%, value > 1Cr
 *   Sell : Close below 200 DMA + 5%
 *   rebalance: everyday @ EOD
 */
void
Invest::Dailyma::dma_alokjain_2(nlohmann::json& ret, nlohmann::json& config)
{
  uint32_t num_periods = config["dma_periods"].size();
  uint32_t value_mark = config["value_mark"].get<double>();

  std::vector<std::string> symbols, dates;

  /* */
  for (auto& symb: config["allsymbols"]) symbols.push_back(symb);

  /* compute dma for all */
  ret["dma"] = nlohmann::json::object();
  for (nlohmann::json::iterator symbit = config["symbols"].begin();
       symbit != config["symbols"].end(); ++symbit) {
    const auto symbol = symbit.key();
    const auto sval = symbit.value();

    /* */
    if (std::find(symbols.begin(), symbols.end(), symbol) == symbols.end()) continue;
    ret["dma"][symbol] = nlohmann::json::array();
    for (uint32_t ip=0; ip<num_periods; ip++) {
      uint32_t period = config["dma_periods"][ip].get<double>();
      std::vector<double> vals;

      uint32_t length = sval["ohlc"].size();
      ret["dma"][symbol][ip] = nlohmann::json::array();
      for (uint32_t i=0; i<length; i++) {
        double o = i ? vals.back() : 0;
        double n = sval["ohlc"][i]["c"].get<double>();

        vals.push_back(n);
        double dma = calcdma(vals, i+1, period);
        ret["dma"][symbol][ip].push_back(dma);

        if (0 == ip) {
          const auto k {sval["ohlc"][i]["dt"].get<std::string>()};
          dates.push_back(k);
        }
      }
    }
  }

  /* */
  uint32_t momentum_day_inc = -1;
  int32_t week_day = -1;
  dayspec2inc(week_day, momentum_day_inc, config["momentum_dayspec"].get<std::string>());

  /* for each date, find the top 20 momentum stocks */
  ret["momentum"] = nlohmann::json::object();
  for (uint32_t di=0; di<dates.size(); di += momentum_day_inc) {
    /* check day of week */
    const auto date = dates[di];
    if (-1 != week_day) {
      auto const dnfd = dayNumFromDate( std::stoi(date.substr(0,4)),
                                        std::stoi(date.substr(5,2)),
                                        std::stoi(date.substr(8,2)) );
      if ( ((dnfd+3)%7) != week_day ) continue;
    }
    uint32_t odi = (di <= 246) ? 0 /* on average 1 yr contains 247 working days */ :
      (di-247);

    /* leave first 200 dates away */
    calcmomentum(ret["momentum"][date], symbols, config, di, odi);
  }
}

void
Invest::Dailyma::dayspec2inc(int32_t& week_day, uint32_t& momentum_day_inc,
                             const std::string& momentum_dayspec)
{
  if ("every-friday-of-week" == momentum_dayspec) {
    momentum_day_inc = 1;
    week_day = 5; /* friday */
  } else if ("every-5th-day" == momentum_dayspec) {
    momentum_day_inc = 5;
    week_day = -1;
    // } else if ("first-of-every-month" == momentum_dayspec) {
    //   momentum_day_inc = 1;
    //   week_day = -1;
  } else {
    momentum_day_inc = 1;
    week_day = -1;
  }
}
