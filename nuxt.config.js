import colors from 'vuetify/es5/util/colors'

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  ssr: false,

  // Env
  env: {
    is_dev: process.env.NODE_ENV == 'development',
    is_prod: process.env.NODE_ENV == 'production',
    is_prod_gitlab:('CI' in process.env),
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - naren"s stockinvest',
    title: 'naren"s stockinvest',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '@/plugins/axios' },
    { src: '@/plugins/stockdata' },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  router: {
    base: (process.env.NODE_ENV === 'production') ? '/stockinvest/' : ''
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    proxy: false,
    withCredentials: true,
    //baseURL: (process.env.NODE_ENV === 'production') ? /* 'http://stocks.greathumanmovement.org' */ 'https://ss.dollane.com' : /*'http://stocks.greathumanmovement.org'*/ /* 'https://ss.dollane.com' */ 'http://127.0.0.1:8080' /* '/mocknseindia/com'*/,
    baseURL: ('CI' in process.env) ? 'https://ss.dollane.com' : ( (process.env.NODE_ENV == 'development') ? 'http://127.0.0.1:8080' : ''),
  },

  /* */
  serverMiddleware: [
    { path: "/mocknseindia/com", handler: "~/api/mock.js" },
  ],

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: colors.purple,
          accent: colors.shades.black,
          secondary: colors.grey.darken1,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.red.accent3,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }

}
