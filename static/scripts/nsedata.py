import tkinter
import tkcalendar
import threading, queue
import os
import webbrowser
import pandas
import pickle
import json
from http.server import HTTPServer, BaseHTTPRequestHandler
from datetime import date, timedelta, datetime
from jugaad_data.nse import index_csv, index_df
from jugaad_data.nse import NSELive

class MyCalendar(tkcalendar.Calendar):
    def _next_month(self):
        tkcalendar.Calendar._next_month(self)
        self.event_generate('<<CalendarMonthChanged>>')
    def _prev_month(self):
        tkcalendar.Calendar._prev_month(self)
        self.event_generate('<<CalendarMonthChanged>>')
    def _next_year(self):
        tkcalendar.Calendar._next_year(self)
        self.event_generate('<<CalendarMonthChanged>>')
    def _prev_year(self):
        tkcalendar.Calendar._prev_year(self)
        self.event_generate('<<CalendarMonthChanged>>')
    def get_displayed_month_year(self):
        return self._date.month, self._date.year

class ExampleGUI(tkinter.Tk):
    def __init__(self, queue, url):
        super().__init__();
        self.queue = queue;
        self.title('Cal');
        self.geometry('600x400');
        #
        self.cal = MyCalendar(self, selectmode='day');
        self.cal.pack();
        self.cal.bind('<<CalendarMonthChanged>>', self.on_change_month)
        self.my_button = tkinter.Button(self, text='Check Nse', command=self.grab_nsequote);
        self.my_button.pack(pady=20);
        self.my_label = tkinter.Label(self, text='');
        self.my_label.pack(pady=20);
        # bind to the virtual event
        self.bind("<<DataAvailable>>", self.poll_queue);
        # browser
        self.web_button = tkinter.Button(self, text="Open Browser", command=lambda url=url:webbrowser.open_new(url));
        self.web_button.pack(pady=20);
        #
        self.nselive = NSELive();

    def on_change_month(self, event):
        # remove previously displayed events
        self.cal.calevent_remove('all')
        year, month = self.cal.get_displayed_month_year()
        # display the current month events 
        # ...
        print(year, month)

    def grab_nsequote(self):
        nseout, status = None, None;
        nseout = self.nselive.market_status();
        if nseout and ('marketState' in nseout):
            status = next(filter(lambda x: x['market'] == 'Capital Market',  nseout['marketState']), None);
        self.my_label.config(text="%s, %s, %s" % (status['tradeDate'], status['last'], status['marketStatus']));

    def poll_queue(self, event):
        while not queue.empty():
            data = self.queue.get_nowait();

    def start(self):
        self.mainloop()

class ExampleHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        global queue, gui;
        if self.path.startswith('/'): self.path = self.path[1:];
        if (self.path == ''): self.path = 'index.html';
        bdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)));
        fpath = os.path.join(bdir, self.path.replace('stockinvest/', '').replace('/', os.sep));
        # queue.put("anything you want")
        # gui.event_generate("<<DataAvailable>>");
        if os.path.isdir(self.path):
            try:
                self.send_response(200)
                self.end_headers()
                self.wfile.write(str(os.listdir(self.path)).encode())
            except Exception:
                self.send_response(500)
                self.end_headers()
                self.wfile.write(b'error')
        elif os.path.isfile(fpath):
            try:
                with open(fpath, 'rb') as f:
                    data = f.read()
                self.send_response(200)
                self.end_headers()
                self.wfile.write(data)
            except FileNotFoundError:
                self.send_response(404)
                self.end_headers()
                self.wfile.write(b'not found')
            except PermissionError:
                self.send_response(403)
                self.end_headers()
                self.wfile.write(b'no permission')
            except Exception:
                self.send_response(500)
                self.end_headers()
                self.wfile.write(b'error')
    def do_POST(self):
        global queue, gui;
        nds = NseDataStore();
        pds = self.rfile.read(int(self.headers['Content-Length']));
        pdj = json.loads(pds);
        if ('/api/strategyAnalysis' == self.path):
            conf = json.loads(pdj['config']);
            sdate, edate = datetime.strptime(conf['startdate'], '%Y-%m-%d').date(), datetime.strptime(conf['enddate'], '%Y-%m-%d').date();
            nds.updateStoreQuote(stocks[0], start=sdate, end=edate);
            self.send_response(200);
            self.send_header('Content-Type', 'application/json');
            self.end_headers();
            retj = stocks[0]['df'].reset_index().rename(columns={'HistoricalDate': 'dt', 'INDEX_NAME': 'name', 'OPEN': 'o', 'HIGH': 'h', 'LOW': 'l', 'CLOSE': 'c'}).to_json(orient='records', date_format='iso');
            self.wfile.write(retj.encode(encoding='utf_8'));
        elif ('/api/nowQuote' == self.path):
            self.send_response(200);
            self.send_header('Content-Type', 'application/json');
            self.end_headers();
            nseout = NSELive().market_status();
            if nseout and ('marketState' in nseout):
                status = next(filter(lambda x: x['market'] == 'Capital Market',  nseout['marketState']), None);
            retj = json.dumps({
                "dt":datetime.strptime(status['tradeDate'], '%d-%b-%Y').strftime('%Y-%m-%d'),
                "name":pdj['symbol'],
                "o":status['last'],
                "h":status['last'],
                "l":status['last'],
                "c":status['last'],
            });
            self.wfile.write(retj.encode(encoding='utf_8'));

class ExampleServer(threading.Thread):
    def __init__(self, host, port, gui, queue):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.gui = gui
        self.queue = queue
        self.daemon = True

    def run(self):
        server = HTTPServer((self.host, self.port), ExampleHandler);
        print(f"Listening on http://%s:%s\n" % server.socket.getsockname());
        server.serve_forever();

## { "dt": "2016-01-01", "o": 11396.95, "h": 11455, "l": 11200, "c": 11321.95,
##   "totqty": 400, "totvalue": 4546007.9, "tottrades": 159 }
class NseDataStore:
    def updateStoreDateCsv(self, symname, frdate, endate, fname):
        index_csv(symbol=symname, from_date=frdate,
                  to_date=endate, output=fname);
    def updateStoreYearCsv(self, stock, year):
        fname = os.path.join(bpath, "%s_%d.csv" % (stock['name'], year));
        if not os.path.isfile(fname):
            self.updateStoreDateCsv(stock['symbol'], date(year, 1, 1), date(year, 12, 31), fname);
        readdf = pandas.read_csv(fname);
        readdf['HistoricalDate'] = pandas.to_datetime(readdf['HistoricalDate']);
        readdf.set_index('HistoricalDate', inplace=True);
        stock['df'] = pandas.concat([stock['df'], readdf], ignore_index=False).drop_duplicates();
    def updateStoreQuote(self, stock, start=date(2015, 1, 1), end=date.today()):
        for year in range(start.year, end.year):
            self.updateStoreYearCsv(stock, year);
        ## Update current year and cache data
        fname = os.path.join(bpath, "%s_%d.pkl" % (stock['name'], end.year));
        if os.path.isfile(fname):
            stat_info = os.stat(fname);
            creation_time = str(datetime.fromtimestamp(stat_info.st_ctime))[:10];
            if creation_time != str(end)[:10]:
                os.remove(fname);
        ## try load data
        dfl = None;
        if True: # not os.path.isfile(fname):
            dfl = index_df(stock['symbol'], date(end.year, 1, 1), end).drop(columns='Index Name').set_index('HistoricalDate');
            with open(fname, 'wb') as f:
                pickle.dump(dfl, f, pickle.HIGHEST_PROTOCOL);
        else:
            with open(fname) as f:
                dfl = pickle.load(f);
        stock['df'] = pandas.concat([stock['df'], dfl], ignore_index=False).drop_duplicates()
        ## Concat last 30 days because of website issues
        if True:
            dfl = index_df(stock['symbol'], end-timedelta(days=30), end).drop(columns='Index Name').set_index('HistoricalDate');
            print (dfl);
        stock['df'] = pandas.concat([stock['df'], dfl], ignore_index=False).drop_duplicates()

### Download as pandas dataframe
if __name__ == "__main__":
    stocks = [
        { 'name': 'Nifty50', 'symbol': 'NIFTY 50', 'df': pandas.DataFrame() },
        { 'name': 'NiftyBank', 'symbol': 'NIFTY BANK', 'df': pandas.DataFrame() },
    ];
    bpath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data');
    if not os.path.exists(bpath):
        os.makedirs(bpath);

    ## if invoked as CGI
    if "DEBUG" in os.environ:
        nds = NseDataStore();
        nds.updateStoreQuote(stocks[0], start=date(2023, 10, 15), end=date.today());
        df = stocks[0]['df'].reset_index().rename(columns={'HistoricalDate': 'dt', 'INDEX_NAME': 'name', 'OPEN': 'o', 'HIGH': 'h', 'LOW': 'l', 'CLOSE': 'c'});
        print (df.reindex(index=df.index[::-1]).to_json(orient='records', date_format='iso'));
        # print (df.to_json(orient='records', date_format='iso'));
        # quotes = stocks[0]['df'].reset_index().rename(columns={'HistoricalDate': 'dt', 'INDEX_NAME': 'name', 'OPEN': 'o', 'HIGH': 'h', 'LOW': 'l', 'CLOSE': 'c'}).to_dict('records');
    else:
        ## GUI
        queue = queue.Queue();
        gui = ExampleGUI(queue, "http://localhost:8910");
        server = ExampleServer("localhost", 8910, gui, queue);
        server.start();
        gui.start();

