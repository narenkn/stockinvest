import { SuperTrend, Stochastic, RSI, SMA } from '@debut/indicators';

/* Need to convert to
   {
   "y" : "OI",
   "series" : [
   {"name":"15000","values":[2.6,2.6,2.6,2.6]},
   {"name":"15100","values":[2.9,3,3.1,3.2]}
   ],
   "dates": ["2000-01-01","2000-02-01","2000-03-01","2000-04-01"]
   };
*/
function populateChart(symbol, pce, div, query,  itm_strikes=0) {
  let mdata = {y: `${symbol} ${pce} OI`, series: [], dates: []};

  /* first get the dates */
  let dates = Object.keys(query);
  dates.sort((a,b) => {
    return (new Date(a) - new Date(b));
  });
  mdata.y += '@' + dates[dates.length-1];

  /* get strikes */
  let strikes = query[dates[dates.length-1]][pce].map(a => a[4]);
  strikes = [...new Set(strikes)];
  strikes.sort((a, b) => (+a) - (+b));

  /* */
  let series = [];
  strikes.forEach(s => {
    let v = [];
    let f = null;
    dates.forEach(d => {
      let o = query[d][pce].find(a => (a[4] == s));
      if (o && (f === null)) f = o[14]/div;
      v.push(o && f ? o[14]/div - f : 0);
    });
    series.push({name: s.toString(), values: v});
  });
  mdata.dates = dates;

  if (itm_strikes > 0) { /* only atm strikes */
    let underlyingValue = (pce == 'CE') ? Object.keys(query).map(qi => query[qi][pce][0][16]).reduce((a, b) => Math.max(a, b), -Infinity) :
        Object.keys(query).map(qi => query[qi][pce][0][16]).reduce((a, b) => Math.min(a, b), Infinity);
    underlyingValue += (pce == 'CE') ? itm_strikes : -itm_strikes;
    let all_series = (pce == 'CE') ?
        series.map(a => a.name).sort((a,b) => parseInt(b)-parseInt(a)).filter(a => parseInt(a) < underlyingValue).slice(0, 30):
        series.map(a => a.name).sort((a,b) => parseInt(b)-parseInt(a)).filter(a => parseInt(a) > underlyingValue).slice(-30);
    mdata.series = series.filter(s => all_series.includes(s.name));
  } else { /* get last 10 OIs of importance */
    let all_series = series.map(a => a.values[a.values.length-1]).sort((a,b) => b-a).slice(0, 20);
    mdata.series = series.filter(s => all_series.includes(s.values[s.values.length-1]));
  }

  return mdata;
};

function populateUnderlying(symbol, cedata, pedata, query, halfSigma, ana_itmce, ana_itmpe) {
  let uldata = { y: symbol, series: [],
                 dates: JSON.parse(JSON.stringify(cedata['dates'])) };
  uldata.y += '@' + cedata['dates'][cedata['dates'].length-1];
  let v1 = [], v2 = [], v3 = [], v4 = [], v5 = [];
  for (let didx=0; didx<uldata.dates.length; didx++) {
    /* underlying */
    let ul = query[uldata.dates[didx]]['CE'][0][16];
    v1.push(ul);
    /* CE */
    let max = 0, itmce = ul;
    for (let si=0; si<cedata.series.length; si++) {
      let tm = Math.max(... cedata.series[si].values.slice(0, didx+1));
      if (tm > max)
        max = tm;
      const tmpce = parseInt(cedata.series[si].name);
      if (itmce < tmpce) itmce = tmpce;
    }
    for (let si=0; si<cedata.series.length; si++) {
      if (cedata.series[si].values[didx] > (0.1 * max)) {
        const tmpce = parseInt(cedata.series[si].name);
        if ((itmce > tmpce) && (tmpce > (ul-(15*halfSigma)))) itmce = tmpce;
      }
    }
    v3.push(itmce - halfSigma);
    /* PE */
    let itmpe = ul;
    max = 0;
    for (let si=0; si<pedata.series.length; si++) {
      let tm = Math.max(... pedata.series[si].values.slice(0, didx+1));
      if (tm > max)
        max = tm;
      const tmppe = parseInt(pedata.series[si].name);
      if (tmppe < itmpe) itmpe = tmppe;
    }
    for (let si=0; si<pedata.series.length; si++) {
      if (pedata.series[si].values[didx] > (0.1 * max)) {
        const tmppe = parseInt(pedata.series[si].name);
        if ((itmpe < tmppe) && (tmppe < (ul+(15*halfSigma)))) itmpe = tmppe;
      }
    }
    v2.push(itmpe + halfSigma);
    /* show itm */
    v4.push(ana_itmpe + halfSigma);
    v5.push(ana_itmce - halfSigma);
  }
  /* take care of random values more than 1 sigma */
  const avg = v1.reduce((s, a) => s+a, 0) / v1.length;
  const lowline = avg - (10*halfSigma), highline = avg + (10*halfSigma);
  for (let i=0; i<v1.length; i++) {
    if ((v1[i] <= lowline) || (v1[i] >= highline)) {
      v1[i] = avg;
    }
    if ((v2[i] <= lowline) || (v2[i] >= highline)) {
      v2[i] = avg;
    }
    if ((v3[i] <= lowline) || (v3[i] >= highline)) {
      v3[i] = avg;
    }
    if (v4[i] <= halfSigma) {
      v4[i] = avg;
    }
    if (v5[i] <= halfSigma) {
      v5[i] = avg;
    }
  }
  uldata.series.push({name: symbol, values: v1});
  uldata.series.push({name: 'High Line', values: v2});
  uldata.series.push({name: 'Low Line', values: v3});
  uldata.series.push({name: 'ITM High Line', values: v4});
  uldata.series.push({name: 'ITM Low Line', values: v5});

  return uldata;
}

/* Need to convert to
   {
   "y" : "OI",
   "series" : [
   {"name":"15000","values":[2.6,2.6,2.6,2.6]},
   {"name":"15100","values":[2.9,3,3.1,3.2]}
   ],
   "dates": ["2000-01-01","2000-02-01","2000-03-01","2000-04-01"]
   };
*/
function populateTotalChart(symbol, cedata, pedata) {
  let totaldata = { y: 'OI Total', series: [],
                    dates: JSON.parse(JSON.stringify(cedata['dates'])) };
  totaldata.y += '@' + cedata['dates'][cedata['dates'].length-1];

  /* NIFTY: get sum & subtract it from others */
  let v = [];
  let f = null;
  for (let i = 0; i < totaldata.dates.length; i++) {
    let s = cedata['series'].map((a) => a.values[i]).reduce((a,b) => a+b);
    if (f === null) f = s;
    v.push(s-f);
  }
  totaldata.series.push({name: `${symbol}-CE`, values: v});
  v = [];
  f = null;
  for (let i = 0; i < totaldata.dates.length; i++) {
    let s = pedata['series'].map((a) => a.values[i]).reduce((a,b) => a+b);
    if (f === null) f = s;
    v.push(s-f);
  }
  totaldata.series.push({name: `${symbol}-PE`, values: v});

  return totaldata;
}

function computeLoading(totaldata) {
  /* compute loading info */
  const cesum = totaldata.series[0].values.reduce((s, a) => s+a, 0);
  const pesum = totaldata.series[1].values.reduce((s, a) => s+a, 0);
  return { PCR: pesum / cesum, CPR: cesum / pesum  };
}

function itm_analysis(pce, pcedata) {
  let maxois = [];
  const arrsz = pcedata.series.length;
  for (let idx=0; idx<arrsz; idx++) {
    maxois.push(Math.max(... pcedata.series[idx].values));
  }
  const maxoi = Math.max(... maxois);
  const residx = maxois.reduce((r, ioi, idx) => {
    if ('CE' == pce) {
      return (ioi > (maxoi*0.1)) ? (
        parseInt(pcedata.series[idx].name) < parseInt(pcedata.series[r].name) ? idx : r
      ) : r;
    }
    /* PE */
    return (ioi > (maxoi*0.1)) ? (
      parseInt(pcedata.series[idx].name) > parseInt(pcedata.series[r].name) ? idx : r
    ) : r;
  }, 0);
  return parseInt(pcedata.series[residx].name);
}

function sma(quotes, interval=2, sidx='sma') {
  for (let idx=0; idx < quotes.length; idx++) {
    if (sidx in quotes[idx]) continue; /* already computed */
    let sum = 0;
    for (let uidx=(idx < (interval-1)) ? 0 : idx+1-interval; uidx<idx+1; uidx++)
      sum += quotes[uidx].c;
    quotes[idx][sidx] = sum / (idx < (interval-1)) ? idx : interval;
  }
}

function max(quotes, interval=2, sidx='max') {
  for (let idx=0; idx < quotes.length; idx++) {
    if (sidx in quotes[idx]) continue;
    if (idx < (interval-1)) {
      quotes[idx][sidx] = quotes[idx].c;
      continue;
    }
    let max = 0;
    for (let uidx=idx+1-interval; uidx<idx+1; uidx++)
      max = Math.max(max, quotes[uidx].c);
    quotes[idx][sidx] = max;
  }
}

function min(quotes, interval=2, sidx='min') {
  for (let idx=0; idx < quotes.length; idx++) {
    if (sidx in quotes[idx]) continue;
    if (idx < (interval-1)) {
      quotes[idx][sidx] = quotes[idx].c;
      continue;
    }
    let min = 0;
    for (let uidx=idx+1-interval; uidx<idx+1; uidx++)
      min = Math.min(min, quotes[uidx].c);
    quotes[idx][sidx] = min;
  }
}

function toMonthlyYearlyTable(tdata, tmdata, tydata) {
  Object.keys(tdata).forEach(k => {
    const yds = [... new Set(tdata[k].map(a => a.date.slice(0, 4)))].sort();
    const mds = [... new Set(tdata[k].map(a => a.date.slice(0, 7)))].sort();
    tmdata[k] = [];
    tydata[k] = [];
    yds.forEach(yk => {
      const d = tdata[k].filter(a => (a.points != 0) && a.date.includes(yk));
      let di = {
        monyear: yk,
        trades: d.length,
        points: d.reduce((i, a) => i+a.points, 0),
        ptrades: d.filter(a => a.points > 0).length,
        ltrades: d.filter(a => a.points <= 0).length,
        totdays: d.reduce((i, a) => i+a.days4sale, 0),
      };
      di.avg = (di.points / di.trades).toFixed(2);
      di.avdays = (di.totdays / d.length).toFixed(2);
      di.tratio = (di.ptrades / di.trades).toFixed(2);
      tydata[k].push(di);
    });
    mds.forEach(mk => {
      const d = tdata[k].filter(a => (a.points != 0) && a.date.includes(mk));
      let di = {
        monyear: mk,
        trades: d.length,
        points: d.reduce((i, a) => i+a.points, 0),
        ptrades: d.filter(a => a.points > 0).length,
        ltrades: d.filter(a => a.points <= 0).length,
        totdays: d.reduce((i, a) => i+a.days4sale, 0),
      };
      di.avg = (di.points / di.trades).toFixed(2);
      di.avdays = (di.totdays / d.length).toFixed(2);
      di.tratio = (di.ptrades / di.trades).toFixed(2);
      tmdata[k].push(di);
    });
    let di = {
      monyear: 'Total',
      trades: tydata[k].reduce((i, a) => i+a.trades, 0),
      points: tydata[k].reduce((i, a) => i+a.points, 0),
      ptrades: tydata[k].reduce((i, a) => i+a.ptrades, 0),
      ltrades: tydata[k].reduce((i, a) => i+a.ltrades, 0),
      totdays: tydata[k].reduce((i, a) => i+a.totdays, 0),
    };
    di.avg = (di.points / di.trades).toFixed(2);
    di.avdays = (di.totdays / di.trades).toFixed(2);
    di.tratio = (di.ptrades / di.trades).toFixed(2);
    tydata[k].push(di);
    di = {
      monyear: 'Total',
      trades: tmdata[k].reduce((i, a) => i+a.trades, 0),
      points: tmdata[k].reduce((i, a) => i+a.points, 0),
      ptrades: tmdata[k].reduce((i, a) => i+a.ptrades, 0),
      ltrades: tmdata[k].reduce((i, a) => i+a.ltrades, 0),
      totdays: tmdata[k].reduce((i, a) => i+a.totdays, 0),
    };
    di.avg = (di.points / di.trades).toFixed(2);
    di.avdays = (di.totdays / di.trades).toFixed(2);
    di.tratio = (di.ptrades / di.trades).toFixed(2);
    tmdata[k].push(di);
  });
}

// function ccw(Ax, Ay, Bx, By, Cx, Cy) {
//   return ((Cy-Ay) * (Bx-Ax)) > ((By-Ay) * (Cx-Ax));
// }
// /* Return true if line segments AB and CD intersect */
// function intersect(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy) {
//   return ( (ccw(Ax, Ay, Cx, Cy, Dx, Dy) != ccw(Bx, By, Cx, Cy, Dx, Dy)) &&
//            (ccw(Ax, Ay, Bx, By, Cx, Cy) != ccw(Ax, Ay, Bx, By, Dx, Dy)) );
// }

/* return format
  { tdata:
    [
      { symbol:
        date:
        value:
        is_buy:
        is_sell:
      },
    ],
    daywise => {
      '<date>': {
        buy: [], sell: [], eod: [],
      }
    }
  }
*/
let strategies = {
  indexstrategy1: /* { analysis: 'indexstrategy1',
                       symbols: '{"NIFTY": {"name": "NIFTY", "id": 1} }',
                       sector: 'index',
                       dma_period: 2,
                       circuitdays: 15,
                       circuitpc: {
                         NIFTY: 0.97,
                         BANKNIFTY: 0.95,
                       },
                     },
                   */
  function (quotes, symbol, strategy) {
    /* process */
    quotes.forEach(r => {r.dt = r.dt.substr(0, 10);});
    quotes.sort((a,b) => (a.dt > b.dt) ? 1 : ((a.dt == b.dt) ? 0 : -1));
    sma(quotes, strategy.dma_period);
    max(quotes, strategy.circuitdays);
    /* */
    var reto = {daywise: {}, tdata: []};
    let pdir = 'down', pTrade = null, days4sale = 0;
    for (let ui=1; ui<quotes.length; ui++, days4sale++) {
      const ndir = (quotes[ui].sma > quotes[ui].c) ? 'down' : 'up';
      const breakTrade = ((quotes[ui].max*strategy.circuitpc[symbol]) >= quotes[ui].c);
      const newTrade = (pdir != ndir) && strategy.trades.includes(ndir) && !breakTrade;
      const exitTrade = pTrade && (newTrade || breakTrade);
      if (newTrade || exitTrade) {
        reto.daywise[quotes[ui].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[ui].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[ui].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[ui].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
      pdir = ndir;
    }
    return reto;
  },
  indexstrategy2: /* { analysis: 'indexstrategy2',
                       symbols: '{"NIFTY": {"name": "NIFTY", "id": 1} }',
                       sector: 'index',
                       dma_periods: [2, 3],
                       maxstocksbuy: 1,
                       noiseSigma: 0.001,
                     }
                   */
  function (quotes, symbol, strategy) {
    /* process */
    quotes.forEach(r => {r.dt = r.dt.substr(0, 10);});
    quotes.sort((a,b) => (a.dt > b.dt) ? 1 : ((a.dt == b.dt) ? 0 : -1));
    sma(quotes, strategy.dma_periods[0], 'sma0');
    sma(quotes, strategy.dma_periods[1], 'sma1');
    /* */
    var reto = {daywise: {}, tdata: []};
    let pTrade = null, days4sale = 0;
    for (let ui=1; ui<quotes.length; ui++, days4sale++) {
      const ndir = (quotes[ui].sma0 > quotes[ui].sma1) ? 'down' : 'up';
      const breakTrade = pTrade && ( (pTrade.is_buy && (ndir == 'down')) ||
                                     (pTrade.is_sell && (ndir == 'up')) );
      const newTrade = (!pTrade) && (Math.abs(quotes[ui].sma0 - quotes[ui].sma1) > (quotes[ui].h * strategy.noiseSigma)) && strategy.trades.includes(ndir);
      const exitTrade = pTrade && !newTrade;
      if (newTrade || exitTrade) {
        reto.daywise[quotes[ui].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[ui].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[ui].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[ui].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
    }
    return reto;
  },
  indexstrategy3: /* attempt to combine some strategies
                    { analysis: 'indexstrategy3',
                     symbols: '{"NIFTY": {"name": "NIFTY", "id": 1} }',
                     // symbols: '{"BANKNIFTY": {"name": "BANKNIFTY", "id": 2} }',
                     sector: 'index',
                     strategies: [
                       {
                         analysis: 'indexstrategy1',
                         dma_period: 2,
                         circuitdays: 10,
                         circuitpc: {
                           NIFTY: 0.97,
                           BANKNIFTY: 0.95,
                         },
                         dayincinit: 0,
                         dayinc: 1,
                       },
                       {
                         analysis: 'indexstrategy2',
                         dma_periods: [2, 3],
                         dayincinit: 0,
                         dayinc: 1,
                         noiseSigma: 0.001,
                       },
                     ],
                     funds: 200000,
                     maxstocksbuy: 1,
                   }
                  */
  function (quotes, symbol, strategy) {
    let srets = [];
    strategy.strategies.forEach(st => {
      srets.push( strategies[st.analysis](quotes, symbol, st) );
    });
    /* */
    var reto = {daywise: {}, tdata: []};
    let pdir = null, pdirs = ['down', 'down'], pTrade = null, days4sale = 0;
    for (let ui=1; ui<quotes.length; ui++, days4sale++) {
      const dt = quotes[ui].dt;
      var ndirs = [null, null];
      for (let si=0; si<2; si++) {
        ndirs[si] = (dt in srets[si].daywise) ? srets[si].daywise[dt].dir : ndirs[si] = pdirs[si];
      }
      const ndir = ndirs[0];
      const newTrade = (ndirs[0] == ndirs[1]) && strategy.trades.includes(ndir);
      const exitTrade = pTrade && (ndirs[0] != ndirs[1]);
      if (newTrade || exitTrade) {
        reto.daywise[quotes[ui].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[ui].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[ui].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[ui].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
      pdir = ndir;
      pdirs = ndirs;
    }
    return reto;
  },
  globaltrend0: /* { analysis: 'globaltrend0',
                     symbols: '{"NIFTY": {"name": "NIFTY", "id": 1} }',
                     sector: 'index',
                     dma_periods: [2, 3],
                     maxstocksbuy: 1,
                     sigma => 0.35,
                     sigmaX => 5,
                     period => 20,
                     }
                   */
  function (quotes, symbol, strategy) {
    /* process */
    quotes.forEach(r => {r.dt = r.dt.substr(0, 10);});
    quotes.sort((a,b) => (a.dt > b.dt) ? 1 : ((a.dt == b.dt) ? 0 : -1));
    max(quotes, strategy.circuitdays, strategy.period);
    min(quotes, strategy.circuitdays, strategy.period);
    /* */
    var reto = {daywise: {}, tdata: []};
    let pdir = null, pTrade = null, days4sale = 0;
    for (let ui=1; ui<quotes.length; ui++, days4sale++) {
      const ndir = (quotes[ui-1].c > quotes[ui].c) ? 'down' : 'up'; /* previous day to today */
      const ndir2 = (Math.abs(quotes[ui].c-quotes[ui].max) < Math.abs(quotes[ui].c-quotes[ui].min)) ?
            'up' : 'down';
      const run = Math.abs(quotes[ui-1].c - quotes[ui].c);
      const isTrend = ( (run > (quotes[ui-1].c * strategy.sigma / 100)) &&
                        (run < (quotes[ui-1].c * strategy.sigmaX / 100)) );
      const newTrade = isTrend && (ndir == ndir2) && strategy.trades.includes(ndir) && !pTrade;
      const exitTrade = pTrade && ( (ndir != ndir2) || (ndir != pdir) );
      if (newTrade || exitTrade) {
        reto.daywise[quotes[ui].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[ui].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[ui].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[ui].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
      pdir = ndir;
    }
    return reto;
  },
  nthtrade: /* consider only the Nth trade */
  function (quotes, symbol, strategy) {
    let ret = strategies[strategy.strategies[0].analysis](quotes, symbol, strategy.strategies[0]);
    /* */
    var reto = {daywise: {}};
    const dates = quotes.map(q=>q.dt).sort();
    var nth = 0, bought = false, bought_days = 0;
    dates.forEach(date => {
      if (bought) bought_days++;
      if (! (date in ret.daywise)) return;
      reto.daywise[date] = { dir: ret.daywise[date].dir, buy: [], sell: [], eod: [] };
      ret.daywise[date].sell.forEach(s => {
        if (bought) {
          if (strategy.invert_trades) s[2] = !s[2];
          s[3] = bought_days;
          reto.daywise[date].sell.push(s);
          bought = false;
          nth = 0;
        }
      });
      ret.daywise[date].buy.forEach(b => {
        nth++;
        if (nth == strategy.trade_id) {
          if (strategy.invert_trades) b[2] = !b[2];
          reto.daywise[date].buy.push(b);
          bought = true;
          bought_days = 0;
        }
      });
    });
    /* */
    const dateso = Object.keys(reto.daywise).filter(d => (reto.daywise[d].sell.length > 0) || (reto.daywise[d].buy.length > 0)).sort();
    reto.daywise = Object.fromEntries(dateso.map(d => [d, reto.daywise[d]]));
    reto.tdata = ret.tdata.filter(d => dateso.indexOf(d.date) > -1);
    return reto;
  },
  catchmoves: /* Uses a base strategy and builds on it.
                 wait for N loss making trades, then enter trade,
                 once trade is entered, stay on until profit becomes max.
                 Reduces the number of trades of base strategy.
               */
  function (quotes, symbol, strategy) {
    let ret = strategies[strategy.strategies[0].analysis](quotes, symbol, strategy.strategies[0]);
    /* */
    var reto = {daywise: {}, tdata: []};
    var tpends = [], numLossTrades = 0, inTrade = false, numWins = 0;
    var totProfit = 0, maxProfit=0;
    ret.tdata.forEach(t => {
      let profit = 0;
      if (0 == t.days4sale)
        tpends.push(t);
      else {
        profit = tpends[0].is_buy ? (t.value - tpends[0].value) : (tpends[0].value - t.value);
        const is_profit_trade = (profit >= (tpends[0].value*strategy.lossTradeSigma/100));
        numLossTrades = is_profit_trade ? 0 : numLossTrades + 1;
        // if (!is_profit_trade) numLossTrades++;
      }
      if (inTrade) {
        if (0 == t.days4sale) { /* open */
          reto.tdata.push({
            symbol: t.symbol,
            date: t.date,
            value: t.value,
            is_buy: t.is_buy,
            is_sell: t.is_sell,
            days4sale: t.days4sale,
          });
          if (! (t.date in reto.daywise)) {
            reto.daywise[t.date] = {
              dir: t.is_buy ? 'up' : 'down',
              buy: [],
              sell: [],
              eod: [],
            };
          }
          reto.daywise[t.date].buy.push([t.symbol, t.value, t.is_buy, t.days4sale]);
        } else {
          reto.tdata.push({
            symbol: t.symbol,
            date: t.date,
            value: t.value,
            is_buy: t.is_buy,
            is_sell: t.is_sell,
            days4sale: t.days4sale,
          });
          if (! (t.date in reto.daywise)) {
            reto.daywise[t.date] = {
              dir: t.is_buy ? 'up' : 'down',
              buy: [],
              sell: [],
              eod: [],
            };
          }
          reto.daywise[t.date].sell.push([t.symbol, t.value, t.is_buy, t.days4sale]);
          totProfit += profit;
        }
      }
      if ((numLossTrades >= strategy.numLossTrades) && !inTrade) {
        inTrade = true;
        maxProfit = totProfit;
      } else if (inTrade && (t.days4sale > 0) && (totProfit > maxProfit)) {
        inTrade = false;
        maxProfit = totProfit;
        numLossTrades = 0;
      }
      if (0 != t.days4sale) tpends.shift();
    });
    /* */
    console.assert(tpends.length < 2);
    return reto;
  },
  globaldir: /* Uses a base strategy and builds on it.
                wait for price to be in a band<dirthreshold> near the sma_period, then enter trade.
                region == updown => trade inside band region
                region == upordown => trade in up region or down region and not inside band region
                region == both => trade in all regions
                once trade is entered, stay on until profit becomes max.
                Reduces the number of trades of base strategy.
             */
  function (quotes, symbol, strategy) {
    let ret = strategies[strategy.strategies[0].analysis](quotes, symbol, strategy.strategies[0]);
    sma(quotes, strategy.sma_period, 'gsma');
    /* get gdir */
    let gdir = 'up';
    quotes.forEach(q => {
      const sma_l = q.gsma - (q.gsma * strategy.dirthreshold / 100);
      const sma_h = q.gsma + (q.gsma * strategy.dirthreshold / 100);
      q.gdir = ('updown' == strategy.region) ? 'updown' : gdir;
      if (('up' == gdir) && (q.c < sma_h)) q.gdir = ('upordown' == strategy.region) ? 'updown' : 'down';
      else if (('down' == gdir) && (q.c > sma_l)) q.gdir = ('upordown' == strategy.region) ? 'updown' : 'up';
      if (('up' == gdir) && (q.c < sma_l)) gdir = 'down';
      else if (('down' == gdir) && (q.c > sma_h)) gdir = 'up';
    });
    /* */
    var reto = {daywise: {}, tdata: []};
    var tpends = null;
    ret.tdata.forEach(t => {
      const quote = quotes.find(q => q.dt == t.date);
      if ((0 == t.days4sale) && ((t.is_buy ? 'up':'down') == quote.gdir) && !tpends) {
        reto.tdata.push({
          symbol: t.symbol,
          date: t.date,
          value: t.value,
          is_buy: t.is_buy,
          is_sell: t.is_sell,
          days4sale: t.days4sale,
        });
        if (! (t.date in reto.daywise)) {
          reto.daywise[t.date] = {
            dir: t.is_buy ? 'up' : 'down',
            buy: [],
            sell: [],
            eod: [],
          };
        }
        reto.daywise[t.date].buy.push([t.symbol, t.value, t.is_buy, t.days4sale]);
        console.assert(null === tpends);
        tpends = t;
      } else if (tpends && (t.days4sale > 0)) {
        reto.tdata.push({
          symbol: t.symbol,
          date: t.date,
          value: t.value,
          is_buy: t.is_buy,
          is_sell: t.is_sell,
          days4sale: t.days4sale,
        });
        if (! (t.date in reto.daywise)) {
          reto.daywise[t.date] = {
            dir: t.is_buy ? 'up' : 'down',
            buy: [],
            sell: [],
            eod: [],
          };
        }
        reto.daywise[t.date].sell.push([t.symbol, t.value, t.is_buy, t.days4sale]);
        tpends = null;
      }
    });
    /* */
    return reto;
  },
  oppositeStrategy: /* Uses a base strategy and builds on it.
                Trade on opposite days of the base strategy.
                if Profit, continue to be opposite.
                if Loss on first day, change strategy to follow base strategy until its exit.
                Then go back to opposite way.
             */
  function (quotes, symbol, strategy) {
    let ret = strategies[strategy.strategies[0].analysis](quotes, symbol, strategy.strategies[0]);
    console.log(ret);
    /* get gdir */
    if (true) {
      let pdir = 'up', ndir = 'opposite' /* or same */, inTrade = false;
      quotes.forEach(q => {
        q.dir = null, q.was_closed = false, q.change_dir = false;
        if (q.dt in ret.daywise) { /* trade was closed & new trade opened */
          ndir = 'opposite';
          q.dir = ('up' == ret.daywise[q.dt].dir) ? 'down' : 'up';
          q.was_closed = (ret.daywise[q.dt].buy.length + ret.daywise[q.dt].sell.length) > 1;
          inTrade = true;
        } else if (inTrade && ('opposite' == ndir)) { /* */
          ndir = 'same';
          q.dir = ('up' == pdir) ? 'down' : 'up';
          q.change_dir = true;
          inTrade = false;
        }
        //console.log(q.dt, ", ", q.c, " q.dir:", q.dir, " ndir:", ndir, " change:", q.change_dir);
        pdir = q.dir;
      });
    }
    /* */
    var reto = {daywise: {}, tdata: []};
    let pdir = 'down', pTrade = null, days4sale = 0;
    for (let ui=1; ui<quotes.length; ui++, days4sale++) {
      const ndir = quotes[ui].dir;
      const breakTrade = quotes[ui].change_dir;
      const newTrade = ('up' == quotes[ui].dir) || ('down' == quotes[ui].dir);
      const exitTrade = pTrade && (newTrade || breakTrade);
      if (newTrade || exitTrade) {
        reto.daywise[quotes[ui].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[ui].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[ui].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[ui].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[ui].dt,
          value: quotes[ui].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
      pdir = ndir;
    }
    return reto;
  },
  tradeNthday: /* Uses a base strategy and builds on it.
                  If base strategy is correct on Nth day, then take trade
                */
  function (quotes, symbol, strategy) {
    let ret = strategies[strategy.strategies[0].analysis](quotes, symbol, strategy.strategies[0]);
    /* hash date */
    var tdatedata = {};
    ret.tdata.forEach(t => {
      tdatedata[t.date] = (t.date in tdatedata) ? tdatedata[t.date]+1 : 1;
    });
    /* */
    var day = -1; /* at -1, it wouldnt increment */
    var reto = {daywise: {}, tdata: []};
    const dates = quotes.map(q=>q.dt).sort();
    var tpends = null, ptrade = null;
    dates.forEach(date => {
      if (day >= 0) day++;
      if (!(date in tdatedata)) { /* trade was not taken today */
        if ((day >= strategy.nthday) && ptrade && (0 == ptrade.days4sale) && !tpends) { /* trade has to be taken */
          const quote = quotes.find(q => q.dt == date);
          tpends = {
            symbol: ptrade.symbol,
            date: quote.dt,
            value: quote.c,
            is_buy: ptrade.is_buy,
            is_sell: ptrade.is_sell,
            days4sale: 0,
          };
          reto.tdata.push(tpends);
          if (! (quote.dt in reto.daywise)) {
            reto.daywise[quote.dt] = {
              dir: ptrade.is_buy ? 'up' : 'down',
              buy: [],
              sell: [],
              eod: [],
            };
          }
          reto.daywise[quote.dt].buy.push([ptrade.symbol, quote.c, ptrade.is_buy, 0]);
          // console.log(date, 'trade taken');
        }
      }
      ret.tdata.filter(td => td.date == date).forEach(trade => {
        ptrade = trade;
        if (tpends && (ptrade.days4sale > 0)) { /* trade finish */
          reto.tdata.push({
            symbol: ptrade.symbol,
            date: ptrade.date,
            value: ptrade.value,
            is_buy: ptrade.is_buy,
            is_sell: ptrade.is_sell,
            days4sale: day,
          });
          if (! (ptrade.date in reto.daywise)) {
            reto.daywise[ptrade.date] = {
              dir: ptrade.is_buy ? 'up' : 'down',
              buy: [],
              sell: [],
              eod: [],
            };
          }
          reto.daywise[ptrade.date].sell.push([ptrade.symbol, ptrade.value, ptrade.is_buy, day]);
          tpends = null;
          day = -1;
          // console.log(date, 'trade completed');
        }
      });
      if (ptrade && (0 == ptrade.days4sale)) day = 0; /* trade was entered today */
      // console.log(date, (date in tdatedata), tdatedata[date], day, tpends, ptrade);
    });
    /* */
    return reto;
  },
  supertrend_mtf: /* Supertrend mutli-timeframe */
  function (quotes, symbol, strategy) {
    const mtf1 = new SuperTrend(strategy.mtf1.p, strategy.mtf1.m),
          mtf2 = new SuperTrend(strategy.mtf2.p, strategy.mtf2.m);
    var reto = {daywise: {}, tdata: []}, dir = 0, pTrade = null;
    quotes.forEach(q => {
      q['mtf1'] = mtf1.nextValue(q.h, q.l, q.c);
      q['mtf2'] = mtf1.nextValue(q.h, q.l, q.c);
      if ((! q['mtf1']) || (! q['mtf2'])) return;
      /* exit conditions */
      if ( pTrade && (0 != dir) &&
           ((q['mtf1'].direction != dir) || (q['mtf2'].direction != dir)) ) {
        if (! (q.dt in reto.daywise)) {
          reto.daywise[q.dt] = {
            dir: 'up',
            buy: [],
            sell: [],
            eod: [],
          };
        }
        reto.tdata.push({
          symbol: strategy.symbol,
          date: q.dt,
          value: q.c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: 0,
        });
        reto.daywise[q.dt].sell.push([strategy.symbol, q.c, (1 != dir), 1]);
        pTrade = null;
        dir = 0;
      }
      /* new trade conditions */
      if ((q['mtf1'].direction == q['mtf2'].direction) && (q['mtf2'].direction != dir)) {
        pTrade = {
          symbol: strategy.symbol,
          date: q.dt,
          value: q.c,
          is_buy: (1 == q['mtf2'].direction),
          is_sell: (1 != q['mtf2'].direction),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        if (! (q.dt in reto.daywise)) {
          reto.daywise[q.dt] = {
            dir: 'up',
            buy: [],
            sell: [],
            eod: [],
          };
        }
        reto.daywise[q.dt].buy.push([strategy.symbol, q.c, (1 == q['mtf2'].direction), 1]);
        dir = q['mtf2'].direction;
      }
    });
    /* */
    return reto;
  },
  Stochastic: /*
               */
  function (quotes, symbol, strategy) {
    const indi = new StochasticRSI(strategy.params.k, strategy.params.d);
    var reto = {daywise: {}, tdata: []}, pdir = 'down', pTrade = null, days4sale=0;
    for (let qi=0; qi<quotes.length; qi++, days4sale++) {
      var q = quotes[qi];
      q['stchc'] = indi.nextValue(q.h, q.l, q.c);
      if (!q['stchc']) continue;
      if ((q['stchc'].d >= 20) && (q['stchc'].d <= 80)) continue;
      if ((q['stchc'].k >= 20) && (q['stchc'].k <= 80)) continue;
      /* FIXME: UNFINISHED CODE
       */
      const ndir = quotes[qi].dir;
      const newTrade = ('up' == quotes[qi].dir) || ('down' == quotes[qi].dir);
      const exitTrade = pTrade && newTrade;
      if (newTrade || exitTrade) {
        reto.daywise[quotes[qi].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[qi].c, (ndir == 'up'), 0]] : [],
          sell: exitTrade ? [[symbol, quotes[qi].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[qi].c]] : [],
        };
      }
      if (exitTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[qi].dt,
          value: quotes[qi].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[qi].dt,
          value: quotes[qi].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
      pdir = ndir;
    }
    /* */
    return reto;
  },
  jarrod_rsi2: /* https://www.youtube.com/watch?v=gvzCDqjccLs
                  entry: enter on RSI2 (close) < 10
                  exit: wait for N days, then exit only on profit for M days
                  dir: only long
                */
  function (quotes, symbol, strategy) {
    const indi = new RSI(strategy.params.rsi);
    const maindi = new SMA(strategy.params.slma);
    var reto = {daywise: {}, tdata: []}, pTrade = null, days4sale=0, inprofitdays=0, entrydays=0;
    for (let qi=0; qi<quotes.length; qi++) {
      var q = quotes[qi];
      q['rsi'] = indi.nextValue(q.c);
      q['ma'] = maindi.nextValue(q.c);
      if (!q['rsi']) continue;
      if (pTrade) {
        days4sale++;
        if (pTrade.value >= q.c) inprofitdays = 0;
        else if (pTrade.value < q.c) inprofitdays++;
      }
      /* exit conditions */
      const maup = q['ma'] + (strategy.params.slma_uncertainity * quotes[qi].c);
      const madown = q['ma'] - (strategy.params.slma_uncertainity * quotes[qi].c);
      const ndir = 'up';
      const newTrade_prev = (q['rsi'] < strategy.cond.entry_rsi_lt) && (q.c > maup) && !pTrade;
      entrydays = newTrade_prev ? entrydays+1 : 0;
      const newTrade = newTrade_prev && (entrydays >= strategy.cond.entry_rsi_days);
      const breakTrade = pTrade && (q.c < madown) && strategy.cond.havesl;
      const exitTrade = pTrade && (inprofitdays >= strategy.cond.wait4sale_in_profit) &&
            (days4sale >= strategy.cond.wait4sale);
      if (newTrade || exitTrade || breakTrade) {
        reto.daywise[quotes[qi].dt] = {
          dir: ndir,
          buy: newTrade ? [[symbol, quotes[qi].c, (ndir == 'up'), 0]] : [],
          sell: (exitTrade || breakTrade) ? [[symbol, quotes[qi].c, ! pTrade.is_buy, days4sale]] : [],
          eod: pTrade ? [[symbol, quotes[qi].c]] : [],
        };
      }
      if (exitTrade || breakTrade) {
        reto.tdata.push({
          symbol: symbol,
          date: quotes[qi].dt,
          value: quotes[qi].c,
          is_buy: ! pTrade.is_buy,
          is_sell: ! pTrade.is_sell,
          days4sale: days4sale,
        });
        pTrade = null;
      }
      if (newTrade) {
        pTrade = {
          symbol: symbol,
          date: quotes[qi].dt,
          value: quotes[qi].c,
          is_buy: (ndir == 'up'),
          is_sell: (ndir == 'down'),
          days4sale: 0,
        };
        inprofitdays = 0;
        reto.tdata.push(pTrade);
        days4sale = 0;
      }
    }
    /* */
    return reto;
  },
};

function analyseStrategy(quotes, symbol, strategy) {
  return strategies[strategy.analysis](quotes, symbol, strategy);
}

/*
 */
function strategy2todaywise (quotes, symbol, strategy) {
  var anaret = {daywise: {}, tdata: []}, pTrade = null;
  const ndir = 'up';

  for (let qi=0; qi<quotes.length; qi++) {
    var q = quotes[qi];
    if (q.newTrade || q.exitTrade || q.breakTrade) {
      anaret.daywise[quotes[qi].dt] = {
        dir: ndir,
        buy: q.newTrade ? [[symbol, quotes[qi].c, (ndir == 'up'), 0]] : [],
        sell: (q.exitTrade || q.breakTrade) ? [[symbol, quotes[qi].c, ! pTrade.is_buy, q.days4sale]] : [],

        eod: pTrade ? [[symbol, quotes[qi].c]] : [],
      };
    }
    if (q.exitTrade || q.breakTrade) {
      anaret.tdata.push({
        symbol: symbol,
        date: quotes[qi].dt,
        value: quotes[qi].c,
        is_buy: ! pTrade.is_buy,
        is_sell: ! pTrade.is_sell,
        days4sale: q.days4sale,
      });
      pTrade = null;
    }
    if (q.newTrade) {
      pTrade = {
        symbol: symbol,
        date: quotes[qi].dt,
        value: quotes[qi].c,
        is_buy: (ndir == 'up'),
        is_sell: (ndir == 'down'),
        days4sale: q.days4sale,
      };
      anaret.tdata.push(pTrade);
    }
  }
  /* */
  return anaret;
}

function daywise2chart(config, tradecons, key, alldates, anaret, lastbuy, pdata, tdata, topens, totpoints, tdata_conf, pdata_tstat) {
  /* chart */
  var funds = config.funds, trades = 0;
  var accpoints = 0;
  // var series0 = {name: `${key}.Points`, values: []};
  var series1 = {name: `${key}.Tot`, values: []};
  tdata[key] = [];

  /* */
  alldates.forEach(date => {
    var datepoints = 0;
    if (! (date in anaret.daywise)) {
      // series0.values.push(datepoints);
      series1.values.push(accpoints);
      return;
    }
    //console.log(date);
    anaret.daywise[date].sell.forEach(b => {
      trades++;
      topens[date]--;
      const valarr = lastbuy[key].map(lb => lb.value);
      const value = valarr.reduce((a, b) => a + b, 0) / valarr.length;
      let points = lastbuy[key][0].is_buy ? (b[1] - value) : (value - b[1]);
      console.assert(b[3] >= 1);
      points -= b[3] * tradecons.decaypday * valarr.length;
      points -= tradecons.slippage; /* for tax & stuff */
      datepoints += points, accpoints += points;
      totpoints[date] += points;
      var td = {
        symbol: b[0],
        date: date + ' / ' + lastbuy[key][0].date + ' - ' + b[3],
        value: `${b[1]} / ${lastbuy[key][0].value}`,
        is_buy: lastbuy[key][0].is_buy,
        is_sell: lastbuy[key][0].is_sell,
        count: lastbuy[key][0].count,
        points: points,
        days4sale: b[3],
        accpoints: accpoints,
        trades: trades,
      };
      funds += points * td.count;
      td.funds = funds;
      delete lastbuy[key];
      tdata[key].unshift(td);
      tdata_conf.unshift(td);
    });
    anaret.daywise[date].buy.forEach(b => {
      var td = {
        symbol: b[0],
        date: date,
        value: b[1],
        is_buy: b[2],
        is_sell: ! b[2],
        count: tradecons.lotsize * Math.floor(Math.abs(funds / tradecons.marginperlot)),
        points: 0,
        days4sale: b[3],
        accpoints: 0,
        trades: trades,
      };
      if (td.count == 0) td.count = tradecons.lotsize;
      td.funds = funds;
      if (key in lastbuy) {
        lastbuy[key].push(td);
      } else {
        lastbuy[key] = [td];
      }
      topens[date]++;
    });
    // series0.values.push(datepoints);
    series1.values.push(accpoints);
  });
  pdata.series.push(/*series0, */series1);
  // console.log(pdata);
  // console.log(topens);
  // console.log('total-trades: ' + tdata[key].filter(a => a.is_sell).length);
  // const profit_trades = tdata[key].filter(a => a.is_sell && (a.points > 0));
  // console.log('total-profit-trades: ' + profit_trades.length);
  // console.log('total-profit-average: ' + (profit_trades.reduce((r, a) => r+a.points, 0) / profit_trades.length));
  // const loss_trades = tdata[key].filter(a => a.is_sell && (a.points <= 0));
  // console.log('total-loss-trades: ' + loss_trades.length);
  // console.log('total-loss-average: ' + (loss_trades.reduce((r, a) => r+a.points, 0) / loss_trades.length));
}

/* return format
 */
let strategies2 = {
  jarrod_rsi2: /* https://www.youtube.com/watch?v=gvzCDqjccLs
                  entry: enter on RSI2 (close) < 10
                  exit: wait for N days, then exit only on profit for M days
                  dir: only long
                */
  function (quotes, symbol, strategy) {
    var pTrade=null, inprofitdays=0, days4sale=0, entrydays=0;
    const indi = new RSI(strategy.params.rsi);
    const maindi = new SMA(strategy.params.slma);
    let pTradeValue = 0;
    for (let qi=0; qi<quotes.length; qi++) {
      var q = quotes[qi];
      q['rsi'] = indi.nextValue(q.c);
      q['ma'] = maindi.nextValue(q.c);
      if (!q['rsi']) continue;
      /* exit conditions */
      if (pTrade) {
        days4sale++;
        if (pTrade.value >= q.c) inprofitdays = 0;
        else if (pTrade.value < q.c) inprofitdays++;
      }

      const maup = q['ma'] + (strategy.params.slma_uncertainity * quotes[qi].c);
      const madown = q['ma'] - (strategy.params.slma_uncertainity * quotes[qi].c);
      q.newTrade = (q['rsi'] < strategy.cond.entry_rsi_lt) && (q.c > maup) && !pTrade;
      entrydays = q.newTrade ? entrydays+1 : 0;
      q.newTrade = q.newTrade && (entrydays >= strategy.cond.entry_rsi_days);
      q.breakTrade = pTrade && (days4sale >= strategy.params.sldays) && strategy.cond.havesl;
      q.exitTrade = pTrade && (inprofitdays >= strategy.cond.wait4sale_in_profit) &&
        (days4sale >= strategy.cond.wait4sale);
      if (q.breakTrade || q.exitTrade) {
        pTrade = null;
      }
      if (q.newTrade) {
        pTrade = {value: q.c};
        inprofitdays = 0;
        days4sale = 0;
      }
      q.days4sale = days4sale;
    }
  },
};

function analyseStrategy2(quotes, symbol, strategy) {
  return strategies2[strategy.analysis](quotes, symbol, strategy);
}

export default {
  populateChart,
  populateUnderlying,
  populateTotalChart,
  computeLoading,
  itm_analysis,
  analyseStrategy,
  analyseStrategy2,
  toMonthlyYearlyTable,
  strategy2todaywise,
  daywise2chart,
};
