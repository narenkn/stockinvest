export default function ({ $axios, redirect, app, store }) {
  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status);
    if (code === 400) {
      redirect('/400');
    }
    if (code === 401) {
      // redirect('/auth/logout');
    }
    store.commit("snackbar/pushMessage", {
      message: ('data' in error.response) ? error.response.data : 'Something went wrong, check network...',
      color: "error"
    });
  });
  $axios.onResponse(resp => {
    if (resp.headers['content-type'].toLowerCase().includes('json') && ('status' in resp.data) && ('message' in resp.data) && !resp.data.status) {
      store.commit("snackbar/pushMessage", {
        message: resp.data.message,
        color: "error"
      });
    }
  });
}
