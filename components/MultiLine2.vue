<template>
  <div>
    <div v-multilinesvg2="{data: data}"></div>
  </div>
</template>

<script>
import * as d3 from "d3";
import {nest} from 'd3-collection';

/* Format
data = [
    {
        "date": "20110930",
        "New York": "63.4",
        "San Francisco": "62.7",
        "Austin": "72.2"
    },
    {
        "date": "20111001",
        "New York": "58.0",
        "San Francisco": "59.9",
        "Austin": "67.7"
    },
];
data.columns = [
    "date",
    "New York",
    "San Francisco",
    "Austin"
];
*/

export default {
  data () {
    return {
    }
  },

  props: {
    data: Array,
  },

  components: {
  },

  mounted() {
  },

  methods: {
  },

  directives: {

    multilinesvg2(el, binding, vnode) {

      if (! binding.value.data) return;
      var data = JSON.parse(JSON.stringify(binding.value.data));
      data.columns = JSON.parse(JSON.stringify(binding.value.data.columns));

      var margin = {
          top: 20,
          right: 80,
          bottom: 30,
          left: 50
        },
        width = 900 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

      var parseDate = d3.timeParse("%Y%m%d");

      var x = d3.scaleTime()
        .range([0, width]);

      var y = d3.scaleLinear()
        .range([height, 0]);

      var color = d3.scaleOrdinal(d3.schemeCategory10);

      var xAxis = d3.axisBottom(x);

      var yAxis = d3.axisLeft(y);

      var line = d3.line()
        .curve(d3.curveBasis)
        .x(function(d) {
          return x(d.date);
        })
        .y(function(d) {
          return y(d.temperature);
        });

      d3.select(el).selectAll("*").remove();
      var svg = d3.select(el).append("svg")
                  .attr("viewBox", [0, 0, width + margin.left + margin.right, height + margin.top + margin.bottom])
                  .attr("font-family", "sans-serif")
                  .attr("font-size", 14)
                  .append("g")
                  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      color.domain(Object.keys(data[0]).filter(function(key) {
        return key !== "date";
      }));

      data.forEach(function(d) {
        d.date = parseDate(d.date);
      });

      var yvalues = color.domain().map(function(name) {
        return {
          name: name,
          values: data.map(function(d) {
            return {
              date: d.date,
              temperature: +d[name]
            };
          })
        };
      });

      x.domain(d3.extent(data, function(d) {
        return d.date;
      }));

      y.domain([
        d3.min(yvalues, function(c) {
          return d3.min(c.values, function(v) {
            return v.temperature;
          });
        }),
        d3.max(yvalues, function(c) {
          return d3.max(c.values, function(v) {
            return v.temperature;
          });
        })
      ]);

      var legend = svg.selectAll('g')
        .data(yvalues)
        .enter()
        .append('g')
        .attr('class', 'legend');

      legend.append('rect')
        .attr('x', width - 20)
        .attr('y', function(d, i) {
          return i * 20;
        })
        .attr('width', 10)
        .attr('height', 10)
        .style('fill', function(d) {
          return color(d.name);
        });

      legend.append('text')
        .attr('x', width - 8)
        .attr('y', function(d, i) {
          return (i * 20) + 9;
        })
        .text(function(d) {
          return d.name;
        });

      svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("U");

      var value = svg.selectAll(".value")
        .data(yvalues)
        .enter().append("g")
        .attr("class", "value");

      value.append("path")
        .attr("class", "line")
        .attr("d", function(d) {
          return line(d.values);
        })
        .style("fill", "none")
        .style("stroke", function(d) {
          return color(d.name);
        });

      value.append("text")
        .datum(function(d) {
          return {
            name: d.name,
            value: d.values[d.values.length - 1]
          };
        })
        .attr("transform", function(d) {
          return "translate(" + x(d.value.date) + "," + y(d.value.temperature) + ")";
        })
        .attr("x", 3)
        .attr("dy", ".35em")
        .text(function(d) {
          return d.name;
        });

      var mouseG = svg.append("g")
        .attr("class", "mouse-over-effects");

      mouseG.append("path") // this is the black vertical line to follow mouse
        .attr("class", "mouse-line")
        .style("stroke", "black")
        .style("stroke-width", "1px")
        .style("opacity", "0");
      
      var lines = document.getElementsByClassName('line');

      var mousePerLine = mouseG.selectAll('.mouse-per-line')
        .data(yvalues)
        .enter()
        .append("g")
        .attr("class", "mouse-per-line");

      mousePerLine.append("circle")
        .attr("r", 7)
        .style("stroke", function(d) {
          return color(d.name);
        })
        .style("fill", "none")
        .style("stroke-width", "1px")
        .style("opacity", "0");

      mousePerLine.append("text")
        .attr("transform", "translate(10,3)");

      mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
        .attr('width', width) // can't catch mouse events on a g element
        .attr('height', height)
        .attr('fill', 'none')
        .attr('pointer-events', 'all')
        .on('mouseout', function() { // on mouse out hide line, circles and text
          d3.select(".mouse-line")
            .style("opacity", "0");
          d3.selectAll(".mouse-per-line circle")
            .style("opacity", "0");
          d3.selectAll(".mouse-per-line text")
            .style("opacity", "0");
        })
        .on('mouseover', function() { // on mouse in show line, circles and text
          d3.select(".mouse-line")
            .style("opacity", "1");
          d3.selectAll(".mouse-per-line circle")
            .style("opacity", "1");
          d3.selectAll(".mouse-per-line text")
            .style("opacity", "1");
        })
        .on('mousemove', function(ev) { // mouse moving over canvas
          var mouse = d3.pointer(ev);
          d3.select(".mouse-line")
            .attr("d", function() {
              var d = "M" + mouse[0] + "," + height;
              d += " " + mouse[0] + "," + 0;
              return d;
            });

          d3.selectAll(".mouse-per-line")
            .attr("transform", function(d, i) {
              var xDate = x.invert(mouse[0]),
                  bisect = d3.bisector(function(d) { return d.date; }).right,
                  idx = bisect(d.values, xDate);
              
              var beginning = 0,
                  end = lines[i].getTotalLength(),
                  target = null,
                  pos = null;
  
              while (true){
                target = Math.floor((beginning + end) / 2);
                pos = lines[i].getPointAtLength(target);
                if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                    break;
                }
                if (pos.x > mouse[0])      end = target;
                else if (pos.x < mouse[0]) beginning = target;
                else break; //position found
              }
              
              d3.select(this).select('text')
                .text(y.invert(pos.y).toFixed(2));
              
            return "translate(" + mouse[0] + "," + pos.y +")";
          });
      });

    },

  }

}
</script>
